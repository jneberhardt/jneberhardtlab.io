
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> <html lang="en"> <head> <meta http-equiv="content-type" content="text/html; charset=utf-8">  
<link rel="stylesheet" type="text/css" href="style.css">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<title>Jens Niklas Eberhardt</title> </head> 
<script type="text/javascript" async src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-MML-AM_CHTML">
  MathJax.Hub.Config({
    tex2jax: {
      inlineMath: [
        ['$', '$'],
        ['\\(', '\\)']
      ]
    },
    TeX: {
      equationNumbers: {
        autoNumber: "AMS"
      },
      extensions: ["AMSmath.js", "AMSsymbols.js"],
    },
  });

</script>
<div>
  <h1>
Jens Niklas Eberhardt
</h1>
  <table width="95%">
    <tbody>
      <col width="60%">
        <col width="10%">
          <col width="30%">
            <tr>
              <td>
                I am a postdoc at the <a href="https://www.math.ucla.edu/">UCLA Department of Mathematics</a> with <a href="http://www.math.ucla.edu/~rouquier/">Raphaël Rouquier</a>.
                <br> My PhD advisor was <a href="http://home.mathematik.uni-freiburg.de/soergel/">Wolfgang Soergel</a> at <a href="https://www.math.uni-freiburg.de/?l=en">Mathematisches Institut der Albert-Ludwigs-Universität Freiburg</a>.
              </td>
              <td></td>
              <td><img src="http://math.ucla.edu/~jens/data/photo.png" width="100%" hspace="5">
              </td>

            </tr>
    </tbody>
  </table>
  <section class="nav">
    <h2>Summary</h2>
    <ul>
      <li><a href="#Research">Research</a></li>
      <li><a href="#Teaching">Teaching</a></li>
      <li><a href="#Contact">Contact</a></li>
    </ul>

  </section>
  <section>
    <a id="Research"><h2>Research</h2></a>
    <p>My research area is <i><a href="https://ncatlab.org/nlab/show/geometric+representation+theory">geometric representation theory</a></i>, where I am particulary interested in:</p>
    <ul>
      <li>semisimple algebraic groups and Lie algebras,</li>
      <li>Soergel modules and Koszul duality,</li>
      <li>categories of sheaves and motives,</li>
      <li>six functor formalisms.</li>
    </ul>
    <p>In very broad strokes, the general philosophy behind this area could be described like this:</p>
    <blockquote>
      <i>Representation theory</i> is a branch of mathematics concerned with the study of symmetrical objects, ranging from <a href="https://en.wikipedia.org/wiki/Wallpaper_group">wallpapers with a repeating floral pattern</a> to <a href="http://www.math.columbia.edu/~woit/woit-texastech.pdf"> quantum-mechanical systems</a>      and <a href="https://en.wikipedia.org/wiki/Automorphic_form">automorphic forms</a>. A powerful technique is to turn representation theoretic problems into questions about the shape or geometry of some space; this makes them amenable to methods from
      other areas of mathematics, as <a href="https://en.wikipedia.org/wiki/Topology">topology</a>, <a href="https://en.wikipedia.org/wiki/Algebraic_geometry">algebraic</a> or <a href="https://en.wikipedia.org/wiki/Differential_geometry">differential</a>      geometry, and one speaks of <i>geometric representation theory</i>.
    </blockquote>
    <h3>
Publications
</h3>
    <p>
      <i> Mixed Motives and Geometric Representation Theory in Equal Characteristic</i>, Jens Niklas Eberhardt and Shane Kelly, arXiv Preprint, 2016 <a href="https://freidok.uni-freiburg.de/fedora/objects/freidok:12750/datastreams/FILE1/content"> (PDF)</a>
    </p>
    <p><i> Graded and geometric parabolic induction</i>, Jens Niklas Eberhardt, PhD Thesis, 2016 <a href="https://arxiv.org/abs/1603.00327"> (PDF)</a></p>

    <p><i>Computing the Tutte Polynomial of a Matroid from its Lattice of Cyclic Flats</i>, Jens Niklas Eberhardt, The Electronic Journal of Combinatorics, Volume 21, Issue 3, 2014. <a href="http://www.combinatorics.org/ojs/index.php/eljc/article/view/v21i3p47"> (PDF)</a></p>
    <h3>
Slides, Notes and Posters
</h3>
    <p><a href="data/talkmixedsheaves.pdf">Slides</a> motivating and sketching a category of mixed sheaves with coefficients in $\mathbb{F}_p$ constructed in joint work with Shane Kelly (see Publications). They were made for a talk at the Erwin Schroedinger Institute in Vienna (2017).</p>
    <p><a href="data/talkparabolicinduction.pdf">Slides</a> motivating and stating some of the results of my PhD thesis. They were made for the investigation of our Graduiertenkolleg in June 2016 and also
      partly used in talks I gave in Regensburg (July 2016), Bonn (August 2016), Clermont-Ferrand (2017) and in my PhD defense.</p>
    <p>Handwritten <a href="data/talkverdier.pdf">notes</a> (thanks to <a href="http://www.konradvoelkel.com/">Konrad Voelkel</a>) of a joint talk with Florian Beck, explaining the relation between Verdier duality, Borel&ndash;Moore homology and cosheaves.</p>
    <p><a href="data/slidesmatroid.pdf">Slides</a> describing the content of my master thesis developing a new algorithm for the computation of the Tutte polynomial of a matroid.</p>
  </section>
  <section>
    <a id="Teaching"><h2>Teaching</h2></a>
    <h3>Current</h3>
    <table class="teaching">
      <tbody>
        <tr>
          <td>Fall 17 (UCLA)</td>
          <td><a href="teaching/F17-31A">Math 31A: Differential and Integral Calculus</a></td>
        </tr>
        <tr>
          <td>Fall 17 (UCLA)</td>
          <td><a href="teaching/F17-115A">Math 115A: Linear Algebra</a></td>
        </tr>
      </tbody>
    </table>
    <h3>Past</h3>
    <table class="teaching">
      <tbody>
        <tr>
          <td>Winter 16/17 (Freiburg)</td>
          <td><a href="data/sl2.pdf">GRK Seminar on "$\operatorname{SL}_2$"</a></td>
        </tr>
        <tr>
          <td>Summer 15 (Freiburg)</td>
          <td><a href="data/sheafcohomology.pdf">GRK Seminar on "Sheaf cohomology"</a></td>
        </tr>
      </tbody>
    </table>
  </section>
  <section>
  <a id="Contact"><h2>Contact</h2></a>
    email: firstname@math.ucla.edu <p>
    <p>
    Math Sciences Building, Room 6304<br>
    520 Portola Plaza <br>
    Los Angeles, CA 90095
    </p>
    Office hours: TBA
  </section>
